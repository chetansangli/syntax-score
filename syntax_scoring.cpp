#include <fstream>
#include <iostream>
#include <stack>
#include <vector>
#include <bits/stdc++.h>
using namespace std;

int get_error_value(char c) {
    switch (c) {
        case '}':
            return 1197;
        case ']':
            return 57;
        case ')':
            return 3;
        case '>':
            return 25137;
        default:
            return 0;
    }
}

char get_matching_pair(char close) {
    switch (close) {
        case '}':
            return '{';
        case ']':
            return '[';
        case ')':
            return '(';
        case '>':
            return '<';
        default:
            return 'x';
    }
}

unsigned long long int do_score(std::string line) {
    std::stack<char> stack;
    std::string::iterator it;
    bool invalid = false;
    unsigned long long int lineCount = 0;
    for (it = line.begin(); it != line.end(); it++) {
        char c = *it;
        if (c == '{' || c == '[' || c == '<' || c == '(') {
            // Push the open bracket to stack
            stack.push(c);
        } else {
            // We have got a close bracket now. Check if its valid.

            // If stack is empty, just return the error
            if (stack.empty()) {
                invalid = true;
                get_error_value(c);
                break;
            } else {
                // Check the top of the stack.
                // Stack top will have the most recently seen open bracket
                // If its a matching pair, pop it and continue the for loop
                // If its not matching, return the error
                char open_bracket = stack.top();
                if (open_bracket != get_matching_pair(c)) {
                     invalid = true;
                     get_error_value(c);
                     break;
                } else {
                    stack.pop();
                }
            }
        }
    }

    //check if line is valid (incomplete only)
    if(!invalid)
    {
        while(stack.size() > 0)
        {
          lineCount *= 5;
          //take the top value from stack and compare to increase linecount
          char c = stack.top();
          stack.pop();
          switch (c) {
               case '(':
                   lineCount += 1; break;
               case '[':
                   lineCount += 2; break;
               case '{':
                   lineCount += 3; break;
               case '<':
                   lineCount += 4; break;
               default:
                   break;
           }
        }

    }

    //return the total line count autocomplete
    return lineCount;
}

int main(int argc, char* argv[]) {
    int final_score = 0;
    std::ifstream file("input.txt");
    vector<unsigned long long int> countAutoComplete;
    int i =0;
    if (file.is_open()) {
        std::string line;
        int line_num = 1;
        while (std::getline(file, line)) {

            unsigned long long int line_score = do_score(line);
            //std::cout << "Score : " << line_score << std::endl;
            if(line_score)
            {
              countAutoComplete.push_back(line_score);
              i++;
            }

            line_num++;
        }

        sort(countAutoComplete.begin(), countAutoComplete.end());

        unsigned long long int middleScore = countAutoComplete.at(floor(countAutoComplete.size()/2));
        std::cout << "middleScore is  : " << middleScore << std::endl;
        file.close();
    }

    return final_score;
}
